#!/usr/bin/bash

# Copyright (c) 2023. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.


# #############################################
# @Author    :   cuikeyu
# @Contact   :   cky2536184321@163.com
# @Date      :   2023/09/21
# @License   :   Mulan PSL v2
# @Desc      :   Test vdo
# #############################################

source "${OET_PATH}"/libs/locallibs/common_lib.sh


function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL "vdo"
    dd if=/dev/zero of=vdo_test_device bs=1M count=10240
    losetup /dev/loop0 vdo_test_device
    vdo create -n my_vdo_volume --device /dev/loop0
    touch /var/log/vdo_activate.log
    LOG_INFO "End to prepare the test environment"
}

function run_test() {
    LOG_INFO "Start to run testcase:oe_test_vdo_00."

    vdo -h | grep  "usage: vdo"
    CHECK_RESULT $? 0 0 "Check vdo -h failed"

    vdo activate -h | grep "usage: vdo activate"
    CHECK_RESULT $? 0 0 "Check vdo activate -h failed"

    vdo activate -a | grep "activated"
    CHECK_RESULT $? 0 0 "Check vdo activate -a failed"

    vdo activate -n my_vdo_volume | grep "activated"
    CHECK_RESULT $? 0 0 "Check vdo activate -n failed"

    vdo activate --verbose -n my_vdo_volume | grep "activated"
    CHECK_RESULT $? 0 0 "Check vdo activate --verbose failed"

    vdo activate -n my_vdo_volume -f /etc/vdoconf.yml | grep "activated"
    CHECK_RESULT $? 0 0 "Check vdo activate -f failed"
    
    vdo activate -n my_vdo_volume --logfile /var/log/vdo_activate.log | grep "activated"
    CHECK_RESULT $? 0 0 "Check vdo activate --logfile failed"
    
    LOG_INFO "End to run test."
}


function post_test() {
    LOG_INFO "Start to restore the test environment."
    sudo rm /var/log/vdo_activate.log
    vdo remove -n my_vdo_volume
    losetup -d /dev/loop0
    rm vdo_test_device -f
    DNF_REMOVE "$@"
    LOG_INFO "End to restore the test environment."
}

main "$@"