#!/usr/bin/bash

# Copyright (c) 2024. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more detaitest -f.

# #############################################
# @Author    :   xiongneng
# @Contact   :   xiongneng@uniontech.com
# @Date      :   2024/02/04
# @License   :   Mulan PSL v2
# @Desc      :   Basic functional verification of vala
# #############################################

source "${OET_PATH}"/libs/locallibs/common_lib.sh

function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL vala
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    cat >  hello.vala << EOF
class HelloWorld : Object {
    static void main(string[] args) {
        stdout.printf("Hello, World!\\n");
    }
}
EOF
    test -f hello.vala
    CHECK_RESULT $? 0 0 "File generation failed"
    valac hello.vala
    test -f hello
    CHECK_RESULT $? 0 0 "File generation failed"
    ./hello > a.log 2>&1
    grep -i "Hello, World!" a.log
    CHECK_RESULT $? 0 0 "Execution failed"
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    rm -rf hello.vala hello a.log
    DNF_REMOVE "$@"
    LOG_INFO "End to restore the test environment."
}

main "$@"