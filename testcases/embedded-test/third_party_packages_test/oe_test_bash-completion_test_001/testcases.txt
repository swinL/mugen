# bashcomp_DATA
2to3 
7z 
a2x 
abook 
aclocal 
acpi 
_adb 
add_members 
alias 
ant 
apache2ctl 
appdata-validate 
apt-build 
apt-cache 
apt-get 
aptitude 
arch 
arp 
arping 
arpspoof 
asciidoc 
aspell 
autoconf 
automake 
autoreconf 
autorpm 
autoscan 
avctrl 
badblocks 
bind 
bk 
brctl 
btdownloadheadless.py 
bts 
bzip2 
_cal 
cancel 
cardctl 
carton 
ccache 
ccze 
cfagent 
cfrun 
chage 
change_pw 
check_db 
check_perms 
checksec 
_chfn 
chgrp 
chkconfig 
chmod 
chown 
chpasswd 
chromium-browser 
chronyc 
chrpath 
_chsh 
cksfv 
cleanarch 
clisp 
clone_member 
complete 
config_list 
configure 
convert 
cowsay 
cpan2dist 
cpio 
cppcheck 
crontab 
cryptsetup 
curl 
cvs 
cvsps 
dd 
deja-dup 
desktop-file-validate 
dhclient 
dict 
_dmesg 
dmypy 
dnssec-keygen 
dnsspoof 
dot 
dpkg 
dpkg-source 
dselect 
dsniff 
dumpdb 
dumpe2fs 
e2freefrag 
e2label 
ebtables 
ecryptfs-migrate-home 
_eject 
eog 
ether-wake 
evince 
explodepkg 
export 
faillog 
fbgs 
fbi 
feh 
file 
file-roller 
filefrag 
filesnarf 
find 
find_member 
fio 
firefox 
flake8 
freebsd-update 
freeciv 
freeciv-server 
function 
fusermount 
gcc 
gcl 
gdb 
genaliases 
gendiff 
genisoimage 
geoiplookup 
getconf 
getent 
gkrellm 
gm 
gnatmake 
gnokii 
gnome-mplayer 
gnome-screenshot 
gpasswd 
gpg 
gpg2 
gpgv 
gphoto2 
gprof 
groupadd 
groupdel 
groupmems 
groupmod 
growisofs 
grpck 
gssdp-discover 
gzip 
hcitool 
hddtemp 
_hexdump 
hid2hci 
hostname 
hping2 
htop 
htpasswd 
hunspell 
_hwclock 
iconv 
id 
idn 
ifstat 
iftop 
ifup 
influx 
info 
inject 
inotifywait 
insmod 
installpkg 
interdiff 
invoke-rc.d 
_ionice 
ip 
ipcalc 
iperf 
ipmitool 
ipsec 
iptables 
ipv6calc 
iscsiadm 
isort 
isql 
iwconfig 
iwlist 
iwpriv 
iwspy 
jar 
jarsigner 
java 
javaws 
jq 
jpegoptim 
jps 
jshint 
json_xs 
jsonschema 
k3b 
kcov 
kill 
killall 
kldload 
kldunload 
koji 
ktutil 
larch 
lastlog 
ldapsearch 
ldapvi 
lftp 
lftpget 
lilo 
links 
lintian 
lisp 
list_admins 
list_lists 
list_members 
list_owners 
_look 
locale-gen 
lpq 
lpr 
lrzip 
lsof 
lsscsi 
lsusb 
lua 
luac 
luseradd 
luserdel 
lvm 
lz4 
lzip 
lzma 
lzop 
macof 
mailmanctl 
make 
makepkg 
man 
mc 
mcrypt 
mdadm 
mdtool 
medusa 
mii-diag 
mii-tool 
minicom 
mkinitrd 
mktemp 
mmsitepass 
_mock 
modinfo 
modprobe 
_modules 
monodevelop 
_mount 
_mount.linux 
mplayer 
mr 
msynctool 
mtx 
munindoc 
munin-node-configure 
munin-run 
munin-update 
mussh 
mutt 
mypy 
mysql 
mysqladmin 
nc 
ncftp 
nethogs 
_newgrp 
newlist 
newusers 
ngrep 
nmap 
_nmcli 
nproc 
nslookup 
nsupdate 
ntpdate 
oggdec 
op 
openssl 
opera 
optipng 
p4 
pack200 
passwd 
patch 
pdftotext 
perl 
perlcritic 
perltidy 
pgrep 
pidof 
pine 
ping 
pkg-config 
pkg-get 
pkg_delete 
pkgadd 
pkgrm 
pkgtool 
pkgutil 
plague-client 
pm-hibernate 
pm-is-supported 
pm-powersave 
pngfix 
portinstall 
portsnap 
portupgrade 
postcat 
postconf 
postfix 
postmap 
postsuper 
povray 
prelink 
printenv 
protoc 
psql 
puppet 
pv 
pwck 
pwd 
pwdx 
pwgen 
pycodestyle 
pydoc 
pydocstyle 
pyflakes 
pylint 
pytest 
python 
pyvenv 
qdbus 
qemu 
qrunner 
querybts 
quota 
radvdump 
rcs 
rdesktop 
remove_members 
removepkg 
_renice 
_repomanage 
reportbug 
_reptyr 
resolvconf 
_rfkill 
ri 
rmlist 
rmmod 
route 
rpcdebug 
rpm 
rpm2tgz 
rpmcheck 
rrdtool 
rsync 
_rtcwake 
_runuser 
sbcl 
sbopkg 
screen 
scrub 
secret-tool 
sh 
shellcheck 
sitecopy 
slackpkg 
slapt-get 
slapt-src 
smartctl 
smbclient 
snownews 
sqlite3 
ss 
ssh 
ssh-add 
ssh-copy-id 
ssh-keygen 
sshfs 
sshmitm 
sshow 
strace 
strings 
_su 
sudo 
svcadm 
svk 
_svn 
_svnadmin 
_svnlook 
sync_members 
synclient 
sysbench 
sysctl 
tar 
tcpdump 
tcpkill 
tcpnice 
timeout 
tipc 
tox 
tracepath 
tshark 
tsig-keygen 
tune2fs 
_udevadm 
ulimit 
_umount 
_umount.linux 
unace 
unpack200 
unrar 
unshunt 
update-alternatives 
update-rc.d 
upgradepkg 
urlsnarf 
useradd 
userdel 
usermod 
valgrind 
vipw 
vmstat 
vncviewer 
vpnc 
watch 
webmitm 
wget 
wine 
withlist 
wodim 
wol 
_write 
wsimport 
wtf 
wvdial 
xdg-mime 
xdg-settings 
xfreerdp 
xgamma 
xhost 
_xm 
xmllint 
xmlwf 
xmms 
xmodmap 
xrandr 
xrdb 
xsltproc 
xvfb-run 
xxd 
xz 
xzdec 
ypmatch 
_yum 
yum-arch 
zopfli 
zopflipng